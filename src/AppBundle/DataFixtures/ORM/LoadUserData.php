<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mt\RestBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('demo');
        $user1->setPassword('demo');

        $user2 = new User();
        $user2->setUsername('restricted');
        $user2->setPassword('restricted');

        $manager->persist($user1);
        $manager->persist($user2);
        $manager->flush();
    }
}

<?php

namespace AppBundle\DependencyInjection;

use Mt\RestBundle\Service\EndpointDefinitionLoader;
use Mt\RestBundle\Service\EndpointServiceInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class AppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        /*$configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);*/

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');

        /** @var EndpointDefinitionLoader $endpointLoader */
//        $endpointLoader = $container->get('app.endpoint.definition_loader');

        /*$loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $locator = $loader->getLocator();
        $path = $locator->locate('endpoints.yml');
        $data = Yaml::parse($path);

        $container->compile();*/

        /** @var EndpointServiceInterface $endpointService */
        /*$endpointService = $container->get('mt_rest.endpoint');
        $endpointService->registerLoader($endpointLoader);*/
    }
}

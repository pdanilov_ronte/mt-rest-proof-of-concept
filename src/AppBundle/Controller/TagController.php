<?php

namespace AppBundle\Controller;

use Mt\RestBundle\Controller\BaseRestController;
use Symfony\Component\HttpFoundation\Request;

class TagController extends BaseRestController
{
    public function __construct()
    {
        $this->path = '/tags';
    }

    public function getTagsAction(Request $request)
    {
        $data = $this->handleRequest($request);

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function postTagAction(Request $request)
    {
        $data = $this->handleRequest($request);

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function getTagAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function putTagAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function deleteTagAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }
}

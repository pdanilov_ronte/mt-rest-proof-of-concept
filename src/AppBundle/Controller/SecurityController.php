<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityController extends Controller
{
    /**
     * @Route(path="/login")
     * @Method({"GET"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $username = $request->get('username');
        $password = $request->get('password');

        if (!empty($username) && !empty($password)) {
            $token = new UsernamePasswordToken($username, $password, 'security');
        } else {
            $token = new AnonymousToken('security', 'Anonymous');
        }

        $manager = $this->get('security.authentication.manager');
        $manager->authenticate($token);

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        $result = $error ? ['error' => $error] : [];

        return new JsonResponse($result);
    }
}

<?php

namespace AppBundle\Controller;

use Mt\RestBundle\Controller\BaseRestController;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends BaseRestController
{
    public function __construct()
    {
        $this->path = '/categories';
    }

    public function getCategoriesAction(Request $request)
    {
        $data = $this->handleRequest($request);

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function postCategoryAction(Request $request)
    {
        $data = $this->handleRequest($request);

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function getCategoryAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function putCategoryAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function deleteCategoryAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }
}

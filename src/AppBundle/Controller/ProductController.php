<?php

namespace AppBundle\Controller;

use Mt\RestBundle\Controller\BaseRestController;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends BaseRestController
{
    public function __construct()
    {
        // @todo less hardcode
        $this->path = '/products';
    }

    public function getProductsAction(Request $request)
    {
        $data = $this->handleRequest($request);

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function postProductAction(Request $request)
    {
        $data = $this->handleRequest($request);

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function getProductAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function putProductAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }

    public function deleteProductAction(Request $request, $id)
    {
        $data = $this->handleRequest($request, ['id' => $id]);

        if (empty($data)) {
            $data = [];
        }

        $view = $this->view($data);

        return $this->handleView($view);
    }
}

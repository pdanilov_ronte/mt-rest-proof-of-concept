<?php

namespace AppBundle\Service\Endpoint;

use Mt\RestBundle\Bridge\Endpoint\DefinitionCollectionInterface;
use Mt\RestBundle\Bridge\Endpoint\DefinitionCollectionTrait;
use Mt\RestBundle\Core\Endpoint\EndpointDefinition;
use Mt\RestBundle\Core\Resource\ResourceDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

class YamlDefinitionCollection implements DefinitionCollectionInterface
{
    use DefinitionCollectionTrait;

    public function __construct(ContainerInterface $container, string $filename)
    {
        $path = $container->get('kernel')->locateResource($filename);

        if (!is_file($path) || !is_readable($path)) {
            throw new \Exception(sprintf('Endpoint definition file "%s" is not readable', $path));
        }

        $data = Yaml::parse(file_get_contents($path));

        foreach ($data as $id => $item) {
            $this->map[$id] = $this->buildEndpointDefinition($item, $container);
        }
    }

    private function buildEndpointDefinition(array $data, ContainerInterface $container)
    {
        if (!isset($data['resource']) || empty($data['resource'])) {
            throw new \Exception('Required parameter "resource" is missing');
        }

        $resourceDefinition = $this->buildResourceDefinition($data['resource'], $container);

        return new EndpointDefinition($data['path'], $resourceDefinition);
    }

    private function buildResourceDefinition(array $data, ContainerInterface $container)
    {
        if (!isset($data['type']) || empty($data['type'])) {
            throw new \Exception('Required parameter "resource type" is missing');
        }
        if (!is_string($data['type']) || (strpos($data['type'], '@') !== 0)){
            throw new \Exception('Invalid parameter "resource type"');
        }

        $typeId = substr($data['type'], 1);
        if (!$container->has($typeId)) {
            throw new \Exception(sprintf('Resource type "%s" not found', $typeId));
        }
        $typeDefinition = $container->get($typeId);
        unset($data['type']);

        return new ResourceDefinition($typeDefinition, $data);
    }
}
